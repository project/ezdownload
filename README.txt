Ezdownload version 1.0

This module is a simple module to provide a tree structure for public downloads.  This module can be easily adapted to accomplish any tree like functionality.  

NOTE: THE UPLOAD MODULE MUST BE ENABLED FOR THIS MODULE TO FUNCTION PROPERLY

In order to achieve maximum functionality, the taxonomy access module or another node based permission module should be installed.  Currently this module has only been tested on PostgreSQL, but you should be able to modify the database install for Mysql, and the module *should* function properly.  This software comes with no guarantees - so use it at your own risk.  

To install this module, follow these simple steps.

1. OBTAIN THE SOURCE AND COPY TO DRUPAL INSTALL
You can obtain the source from the project's webpage at:

http://drupal.org/project/ezdownload

Once you have the source, extract it with a command like:

tar -xvf ezdownload.tar.gz

Then copy that directory to your drupal install with a command like:

cp -r ezdownload /<www>/<http root>/<ht docs>/modules/


2. INSTALL ADDITIONAL TABLES

Change into your ezdownload directory with a command like:

cd /<www>/<http root>/<ht docs>/modules/ezdownload/

Next, On the command prompt run a command like

psql -U <username> -W <database> < database.postgres

make sure your user is the owner of the database, otherwise this script will fail.  

3. ENABLE THE MODULE
Go to administrator -> modules and click on the check box next to ezdown.  Also be sure that box next to the upload module is also checked.  Next Hit the submit button on the bottom.

4. CONFIGURE THE EZDOWNLOAD AND UPLOADS

Next go to administrator -> settings and make sure there are no errors about directories not existing.  Pay special attention to the file system path input box. MAKE SURE THAT DIRECTORY EXISTS AND IS READABLE BY THE WEBSERVER.  If not, create the directory with a command like:

mkdir /<www>/<http root>/<htdocs>/files

On my particular set up I gave the files directory an owner and a group of daemon with a command like:

chown daemon:daemon files

NOTE:  you may need to have root access to change the ownership of a directory

Once the settings page looks okay, go to administrator -> settings -> uploads.  Adjust the maximum file size to your liking.

Then go to administrator -> content -> configure -> content types.
Click on the configure hyperlink next to EZdownload leaf.  At the bottom of the page, under the Attachments header, click Enabled then click save configuration. 

Last, go to administrator -> access controls and set the appropriate permissions.  Usually you want everyone to have the 'access content' permission and only administrator to have 'administer ezdown'.

Also, if you have taxonomy access installed, or a similar module, be sure to set group permissions as described in the documentation.

Thanks for testing this module out!  If you like this module, or would like to see it improve, please contribute!  You don't have to be a programmer to contribute!  All users can post bug reports.

If you need any help installing this module please email me at tmckeown@gmail.com.